package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.entity.*;
import com.devcamp.pizza365.repository.*;
import com.devcamp.pizza365.service.ExcelExporter;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CustomerController {
	@Autowired
	CustomerRepository customerRep;
	@Autowired
	OrderRepository orderRep;
	@Autowired
	OrderDetailRepository orderDetailRep;

	@GetMapping("/allCustomers")
	public ResponseEntity<Object> getAllCustomers() {
		try {
			List<Customer> customer = new ArrayList<Customer>();

			customerRep.findAll().forEach(customer::add);

			return new ResponseEntity<>(customer, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/allOrders")
	public ResponseEntity<Object> getAllOrders() {
		try {
			List<Order> orders = new ArrayList<Order>();

			orderRep.findAll().forEach(orders::add);

			return new ResponseEntity<>(orders, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			// return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/allOrderDetails")
	public ResponseEntity<Object> getAllOrderDetails() {
		try {
			List<OrderDetail> orderDetails = new ArrayList<OrderDetail>();

			orderDetailRep.findAll().forEach(orderDetails::add);

			return new ResponseEntity<>(orderDetails, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/customers/export")
	public void exportToExcel(HttpServletResponse response) throws IOException {
		// Headers .. 
		// Set content type application/octet-stream to stream binary file.
		response.setContentType("application/octet-stream");

		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
		String currentDateTime = dateFormatter.format(new Date());
		String contentDisposition = "attachment; filename=users_"+ currentDateTime+".xlsx";
		response.addHeader("Content-Disposition", contentDisposition);
		
		List<Customer> customers = new ArrayList<Customer>();

		customerRep.findAll().forEach(customers::add);

		ExcelExporter exporter = new ExcelExporter(customers);

		ServletOutputStream stream = response.getOutputStream();
		exporter.export(stream);
	}
	
	/*
	 * @GetMapping("/vouchers/{id}") public ResponseEntity<Order>
	 * getCVoucherById(@PathVariable("id") long id) { Optional<Order> voucherData =
	 * pVoucherRepository.findById(id); if (voucherData.isPresent()) { return new
	 * ResponseEntity<>(voucherData.get(), HttpStatus.OK); } else { return new
	 * ResponseEntity<>(HttpStatus.NOT_FOUND); } }
	 * 
	 * @PostMapping("/vouchers") public ResponseEntity<Object>
	 * createCVoucher(@Valid @RequestBody Order pVouchers) {// Sửa để cho phép
	 * validate try { pVouchers.setNgayTao(new Date());
	 * pVouchers.setNgayCapNhat(null); Order _vouchers =
	 * pVoucherRepository.save(pVouchers); return new ResponseEntity<>(_vouchers,
	 * HttpStatus.CREATED); } catch (Exception e) {
	 * System.out.println("+++++++++++++++++++++::::: " +
	 * e.getCause().getCause().getMessage()); // Hiện thông báo lỗi tra back-end
	 * return new ResponseEntity<>(e.getCause().getCause().getMessage(),
	 * HttpStatus.INTERNAL_SERVER_ERROR); // return
	 * ResponseEntity.unprocessableEntity().body("Failed to Create specified //
	 * Voucher: "+e.getCause().getCause().getMessage()); } }
	 * 
	 * @PutMapping("/vouchers/{id}") public ResponseEntity<Object>
	 * updateCVoucherById(@PathVariable("id") long id, @RequestBody Order pVouchers)
	 * { Optional<Order> voucherData = pVoucherRepository.findById(id); if
	 * (voucherData.isPresent()) { Order voucher = voucherData.get();
	 * voucher.setMaVoucher(pVouchers.getMaVoucher());
	 * voucher.setPhanTramGiamGia(pVouchers.getPhanTramGiamGia());
	 * voucher.setNgayCapNhat(new Date()); try { return new
	 * ResponseEntity<>(pVoucherRepository.save(voucher), HttpStatus.OK); } catch
	 * (Exception e) { return ResponseEntity.unprocessableEntity()
	 * .body("Failed to Update specified Voucher:" +
	 * e.getCause().getCause().getMessage()); }
	 * 
	 * } else { // return new ResponseEntity<>(HttpStatus.NOT_FOUND); return
	 * ResponseEntity.badRequest().body("Failed to get specified Voucher: " + id +
	 * "  for update."); } }
	 * 
	 * @DeleteMapping("/vouchers/{id}") public ResponseEntity<Order>
	 * deleteCVoucherById(@PathVariable("id") long id) { try {
	 * pVoucherRepository.deleteById(id); return new
	 * ResponseEntity<>(HttpStatus.NO_CONTENT); } catch (Exception e) {
	 * System.out.println(e); return new ResponseEntity<>(null,
	 * HttpStatus.INTERNAL_SERVER_ERROR); } }
	 * 
	 * @DeleteMapping("/vouchers") public ResponseEntity<Order> deleteAllCVoucher()
	 * { try { pVoucherRepository.deleteAll(); return new
	 * ResponseEntity<>(HttpStatus.NO_CONTENT); } catch (Exception e) {
	 * System.out.println(e); return new ResponseEntity<>(null,
	 * HttpStatus.INTERNAL_SERVER_ERROR); } }
	 */
}
