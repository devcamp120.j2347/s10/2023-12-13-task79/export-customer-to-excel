package com.devcamp.pizza365.service;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.devcamp.pizza365.entity.Customer;

public class ExcelExporter {
    private Workbook workbook;
    private Sheet activeSheet;

    private List<Customer> customers;

    public ExcelExporter(List<Customer> customers) {
        this.workbook = new XSSFWorkbook();
        this.activeSheet = this.workbook.createSheet("Customers");

        this.customers = customers;
    }

    public ExcelExporter(Workbook workbook, Sheet activeSheet, List<Customer> customers) {
        this.workbook = workbook;
        this.activeSheet = activeSheet;
        this.customers = customers;
    }

    public void writeCell(Row row, int columnIndex, Object value, CellStyle style) {
        this.activeSheet.autoSizeColumn(columnIndex);
        Cell cell = row.createCell(columnIndex);
        cell.setCellStyle(style);

        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof String) {
             cell.setCellValue((String) value);
        }  else if (value instanceof Date) {
             cell.setCellValue((Date) value);
        }  else if (value instanceof Boolean) {
             cell.setCellValue((Boolean) value);
        }
    
    }

    public void writeHeaders() {
        Row row = this.activeSheet.createRow(0);
        CellStyle style = workbook.createCellStyle();
        XSSFFont font = (XSSFFont) this.workbook.createFont();
        font.setBold(true);
        font.setColor(IndexedColors.BLUE.getIndex());
        style.setFillBackgroundColor(IndexedColors.AQUA.getIndex());
        style.setFont(font);

        // Write headers
        writeCell(row, 0, "UserId", style);
        writeCell(row, 1, "First Name", style);
        writeCell(row, 2, "Last Name", style);
        writeCell(row, 3, "Phone Number", style);
        writeCell(row, 4, "Address", style);
        writeCell(row, 5, "City", style);
        writeCell(row, 6, "Country", style);
    }


    public void writeDataLines(){
        int rowCount = 1;
        for(Customer customer: this.customers) {
            Row row = this.activeSheet.createRow(rowCount);
            CellStyle style = workbook.createCellStyle();

            writeCell(row, 0, customer.getId(), style);
            writeCell(row, 1, customer.getFirstName(), style);
            writeCell(row, 2, customer.getLastName(), style);
            writeCell(row, 3, customer.getPhoneNumber(), style);
            writeCell(row, 4, customer.getAddress(), style);
            writeCell(row, 5, customer.getCity(), style);
            writeCell(row, 6, customer.getCountry(), style);

            rowCount++;
        }
    }

    public void export(OutputStream stream) throws IOException {
        // Write headers.
        this.writeHeaders();
        // Write data row
        this.writeDataLines();


        this.workbook.write(stream);
        stream.close();
    }
    
}
